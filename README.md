# Dependencies
 - nix

# Getting started

### Clone project
``` sh
git clone git@gitlab.com:PavelSkorik/purescript-hello-world.git
```

### Enter to project dir and nix-shell
``` sh
cd purescript-hello-world && nix-shell
```

### Build and run
``` sh
spago build && spago run
```

# Setting Visual Studio Code 

### Install "PureScript IDE" extension
```sh
ext install nwolverson.ide-purescript
```
https://marketplace.visualstudio.com/items?itemName=nwolverson.ide-purescript

### Open project in vscode

``` sh
cd purescript-hello-world && nix-shell && code .
```
