{ name = "purescript-hello-world"
, dependencies =
  [ "console"
  , "effect"
  , "prelude"
  , "tuples"
  , "halogen"
  , "halogen-hooks"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs" ]
}
