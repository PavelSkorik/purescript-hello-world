module Main where

import Prelude

import Halogen as H
import Halogen.Hooks as Hooks
import Halogen.HTML.Events as HE
import Halogen.HTML as HH
import Halogen.Aff as HA
import Halogen.VDom.Driver (runUI)
import Effect (Effect)
import Data.Tuple.Nested ((/\))

main :: Effect Unit
main = do
  HA.runHalogenAff do
    body <- HA.awaitBody
    runUI button unit body



button :: forall q i o m. H.Component q i o m
button = Hooks.component \_ _ -> Hooks.do
  count /\ countId <- Hooks.useState 0

  Hooks.pure do
    HH.button
      [ HE.onClick \_ -> Hooks.modify_ countId (_ + 1) ]
      [ HH.text $ show count ]


